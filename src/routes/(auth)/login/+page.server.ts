import { fail, setError, superValidate } from 'sveltekit-superforms';
import { zod } from 'sveltekit-superforms/adapters';
import { loginSchema } from '../schema.js';

export async function load() {
  const form = await superValidate(zod(loginSchema));
  return { form };
}

export const actions = {
  default: async ({ locals, request }) => {
    const form = await superValidate(request, zod(loginSchema));
    if (!form.valid) return fail(400, { form });

    let error;
    try {
      await locals.pb.collection('users').authWithPassword(form.data.email, form.data.password);
    } catch (err) {
      error = err;
    }
    if (error) return setError(form, 'email', 'Invalid email or password');
    return { form };
  }
};
