import { redirect } from '@sveltejs/kit';

export async function POST({ locals }) {
  locals.pb.authStore.clear();
  redirect(302, '/login');
}
