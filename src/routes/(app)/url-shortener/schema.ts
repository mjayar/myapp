import { z } from 'zod';

export const createSchema = z.object({
  url: z.string().url(),
  alias: z.string().min(5),
  dateStart: z.date().optional(),
  dateExpires: z.date().optional()
});

export const deleteSchema = z.object({
  ids: z.string()
});

export const updateSchema = z.object({
  id: z.string(),
  url: z.string().url().optional(),
  alias: z.string().min(5).optional(),
  dateStart: z.date().optional(),
  dateExpires: z.date().optional()
});

export type CreateSchema = typeof createSchema;
export type DeleteSchema = typeof deleteSchema;
export type UpdateSchema = typeof updateSchema;
