export type DataRecord = {
  id: string;
  alias: string;
  url: string;
  active: boolean;
  dateStart?: string;
  dateExpires?: string;
  created: string;
  updated: string;
  userId: string;
  metadata?: Record<string, unknown>;
};
