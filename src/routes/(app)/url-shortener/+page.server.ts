import { error } from '@sveltejs/kit';
import { fail, setError, superValidate } from 'sveltekit-superforms';
import { zod } from 'sveltekit-superforms/adapters';
import { createSchema, deleteSchema, updateSchema } from './schema.js';

export async function load({ locals }) {
  const forms = {
    create: await superValidate(zod(createSchema)),
    update: await superValidate(zod(updateSchema)),
    delete: await superValidate(zod(deleteSchema))
  };

  let data;
  let errors;
  try {
    data = await locals.pb.collection('url_shortener').getFullList({
      sort: '-created'
    });
  } catch (err) {
    errors = err;
  }
  if (errors) error(400);
  return { forms, user: locals.pb.authStore.model, data };
}

export const actions = {
  create: async ({ locals, request }) => {
    const form = await superValidate(request, zod(createSchema));
    if (!form.valid) return fail(400, { form });

    let data;
    let errors;
    try {
      data = await locals.pb.collection('url_shortener').create(form.data);
    } catch (err) {
      errors = err;
    }
    if (errors) return setError(form, '', 'Something went wrong');
    return { form, data };
  },

  delete: async ({ locals, request }) => {
    const form = await superValidate(request, zod(deleteSchema));
    if (!form.valid) return fail(400, { form });

    const ids = form.data.ids.split(',');

    for (let i = 0; i < ids.length; i++) {
      let errors;
      try {
        await locals.pb.collection('url_shortener').delete(ids[i]);
      } catch (err) {
        errors = err;
      }
      if (errors) return setError(form, '', 'Something went wrong');
    }
    return { form };
  },

  update: async ({ locals, request }) => {
    const form = await superValidate(request, zod(updateSchema));
    if (!form.valid) return fail(400, { form });

    const { id, ...data } = form.data;

    let errors;
    try {
      await locals.pb.collection('url_shortener').update(id, data);
    } catch (err) {
      errors = err;
    }
    if (errors) return setError(form, '', 'Something went wrong');
    return { form };
  }
};
